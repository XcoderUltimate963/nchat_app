const http = require('https');
const fs = require('fs');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');

const { addUser, removeUser, getUser, getUserInRoom } = require('./users');

const router = require('./router');

var connectionOptions =  {
    "force new connection" : true,
    "reconnectionAttempts": "Infinity", //avoid having user reconnect manually in order to prevent dead clients after a server restart
    "timeout" : 1000,                  //before connect_error and connect_timeout are emitted.
    "transports" : ["websocket"]
};

const privateKey = fs.readFileSync("rootCA.key", "utf8");
const certificate = fs.readFileSync("rootCA.crt", "utf8");

const credentials = {
    key: privateKey,
    cert: certificate,
    passphrase: process.env.PASSPHRASE
};

const app = express();
const server = http.createServer(/*credentials, */app);
const io = socketio(server, connectionOptions);

app.use(cors());
app.use(router);

io.on('connect', (socket) => {
    socket.on('join', ({ name, room }, callback) => {
        console.log(name+"joined");
        const { error, user } = addUser({ id: socket.id, name, room });
        
        if (error)
            return callback(error);
        
        socket.join(user.room);
        
        socket.emit('message', { user: 'admin', text: `${user.name}, welcome to room ${user.room}.` });
        socket.broadcast().to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined!` });
        
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
        
        callback();
    });
    
    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        
        io.to(user.room).emit('message', { user: user.name, text: message });
        
        callback();
    });
    
    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        
        if (user) {
            io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} has left.` });
            io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
        }
    })
});

server.listen(process.env.PORT || 5000, () => console.log(`Server has started`));
